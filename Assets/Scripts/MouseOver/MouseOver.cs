﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseOver : MonoBehaviour
{
    private Outline outline;

    private void Start()
    {
        outline = gameObject.GetComponent<Outline>();
        outline.enabled = false;
    }

    private void OnMouseEnter()
    {
        outline.enabled = true;
        if (gameObject.CompareTag("Item"))
        {
            EnableTooltip();
        }
    }

    private void OnMouseExit()
    {
        outline.enabled = false;
        if (gameObject.CompareTag("Item"))
        {
            DisableTooltip();
        }
    }

    private void EnableTooltip()
    {
        transform.Find("Tooltip").GetComponent<Tooltip>().Enable();
    }

    private void DisableTooltip()
    {
        transform.Find("Tooltip").GetComponent<Tooltip>().Disable();
    }
}
