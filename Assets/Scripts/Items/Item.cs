﻿public class Item
{
    public string itemName;
    public string icon;

    public Item(string itemName, string icon)
    {
        this.itemName = itemName;
        this.icon = icon;
    }
}
