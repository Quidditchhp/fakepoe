﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    private List<Item> inventory = new List<Item>();
    public List<GameObject> inventoryUI = new List<GameObject>();
    public void PickUpItem(GameObject item)
    {
        if (item.GetComponent<DroppedItem>().pickUpable)
        {
            Type itemType = Type.GetType(item.name);
            Item itemToStore = (Item)Activator.CreateInstance(itemType);
            inventory.Add(itemToStore);
            GameObject.Destroy(item);

            AddToInventoryUI(itemToStore);
        }
    }

    public void AddToInventoryUI(Item item)
    {
        foreach (GameObject slot in inventoryUI)
        {
            if (!slot.transform.GetChild(0).gameObject.activeSelf)
            {
                slot.transform.GetChild(0).gameObject.SetActive(true);
                slot.transform.GetChild(0).gameObject.GetComponent<Image>().sprite = Resources.Load<Sprite>("Icons/" + item.icon);
                break;
            }
        }
    }
}
