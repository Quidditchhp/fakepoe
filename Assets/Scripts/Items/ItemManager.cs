﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class ItemManager
{
    private static ItemManager instance = null;
    private Dictionary<string, Dictionary<string, float>> dropTableMap = new Dictionary<string, Dictionary<string, float>>();

    private ItemManager()
    {
        dropTableMap.Add("Guard", DropTables.GuardLoot());
        dropTableMap.Add("GuardCaptain", DropTables.GuardCaptainLoot());
    }

    public static ItemManager GetInstance()
    {
        if (instance == null)
        {
            instance = new ItemManager();
        }
        return instance;
    }

    public void Drop(string name, float x, float z)
    {
        Vector3 dropPosition = new Vector3(x, 0.1f, z);
        Dictionary<string, float> dropTable = dropTableMap[name];
        foreach (string item in dropTable.Keys)
        {
            int roll = UnityEngine.Random.Range(0, 101);
            if (dropTable[item] >= roll)
            {
                GameObject droppedItem = (GameObject)GameObject.Instantiate(Resources.Load("Items/" + item), dropPosition, new Quaternion());
                droppedItem.name = item;
                break;
            }
        }
    }
}
