﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroppedItem : MonoBehaviour
{
    public bool pickUpable = false;
    void Start()
    {
        StartCoroutine(EnablePickUp());
    }

    IEnumerator EnablePickUp()
    {
        yield return new WaitForSeconds(0.5f);
        pickUpable = true;
    }
}
