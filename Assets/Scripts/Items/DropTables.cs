﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DropTables
{
    public static Dictionary<string, float> GuardLoot()
    {
        return new Dictionary<string, float>()
        {
            { "Axe",10 },
            { "Sword",10 },
        };
    }

    public static Dictionary<string, float> GuardCaptainLoot()
    {
        return new Dictionary<string, float>()
        {
            { "Axe",5 },
            { "Sword",5 },
            { "Armour",10 },
        };
    }
}

