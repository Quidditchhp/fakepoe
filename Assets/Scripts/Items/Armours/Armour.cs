﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armour : Item
{
    public float health;
    public float armour;

    public Armour(string name, string icon, float health, float armour) : base(name,icon)
    {
        this.health = health;
        this.armour = armour;
    }
}
