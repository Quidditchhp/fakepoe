﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : Item
{
    public float damage;
    public float attackSpeed;

    public Weapon(string name, string icon, float damage, float attackSpeed) : base(name, icon)
    {
        this.damage = damage;
        this.attackSpeed = attackSpeed;
    }
}
