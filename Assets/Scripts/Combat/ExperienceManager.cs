﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExperienceManager : MonoBehaviour
{
    private const float baseXPRequirement = 100;
    private float nextLvlXPRequirement = 100;

    private float totalXP = 0;
    private float currentLvlXP = 0;

    private int level = 1;
    public Slider experienceBar;
    public Text experienceText;


    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

    void Update()
    {
        if (currentLvlXP >= nextLvlXPRequirement)
        {
            LevelUp();
        }
    }

    public void Add(float experience)
    {
        totalXP += experience;
        currentLvlXP += experience;

        UpdateXPBar();
    }

    private void LevelUp()
    {
        level++;
        currentLvlXP -= nextLvlXPRequirement;
        nextLvlXPRequirement += baseXPRequirement * ((float)level / 10);

        UpdateXPBar();
    }

    private void UpdateXPBar()
    {
        float xpPercentage = 100 * (currentLvlXP / nextLvlXPRequirement);
        experienceBar.value = xpPercentage;
        experienceText.text = "Level " + level + "(" + xpPercentage + " %)";
    }
}
