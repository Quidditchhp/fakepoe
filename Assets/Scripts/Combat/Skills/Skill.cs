﻿using System.Collections;
using UnityEngine;

public abstract class Skill
{
    private bool onCooldown = false;

    private float damage;
    private float cooldown;
    private float animation;
    private float range;
    private float width;
    public string name;

    public Skill(string name, float damage, float range, float width, float cooldown, float animation)
    {
        this.name = name;
        this.damage = damage;
        this.range = range;
        this.width = width;
        this.cooldown = cooldown;
        this.animation = animation;
    }

    public void Use(Player player, Vector3 mousePosition)
    {
        if (!onCooldown)
        {
            player.animationLocked = true;
            player.StartCoroutine(AttackAnimation(player));
        }
    }

    IEnumerator AttackCooldown()
    {
        yield return new WaitForSeconds(cooldown);
        onCooldown = false;
    }

    IEnumerator AttackAnimation(Player player)
    {
        yield return new WaitForSeconds(animation);

        Vector3 centre = player.transform.position + player.transform.forward * (range / 2);
        Collider[] other = Physics.OverlapBox(centre, new Vector3(width / 2, player.transform.position.y, range / 2), player.transform.rotation, LayerMask.GetMask("Enemies"));

        foreach (Collider c in other)
        {
            HealthManager.GetInstance().DamageEntity(c.gameObject.GetComponent<Enemy>(), damage);
        }

        player.animationLocked = false;
        onCooldown = true;
        player.StartCoroutine(AttackCooldown());
    }
}
