﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeavyStrike : Skill
{
    public HeavyStrike() : base("Heavy Strike", 100, 4, 1, 0, 1)
    {
    }
}
