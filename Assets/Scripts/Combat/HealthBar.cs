﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider slider;
    public Image fillImage;

    private Color fullHealthColor = Color.red;
    private Color zeroHealthColor = Color.black;
    private GameObject camera;
    // Start is called before the first frame update

    private void Start()
    {
        camera = GameObject.FindGameObjectWithTag("MainCamera");
    }

    private void Update()
    {
        transform.rotation = camera.transform.rotation;
    }
    public void UpdateHealth(float percentageHealth)
    {
        // Set the slider's value appropriately.
        slider.value = 100 * percentageHealth;
        // Interpolate the color of the bar between the choosen colours based on the current percentage of the starting health.
        fillImage.color = Color.Lerp(zeroHealthColor, fullHealthColor, percentageHealth);
    }
}
