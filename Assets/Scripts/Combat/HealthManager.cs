﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class HealthManager
{
    private Dictionary<Attackable, float> maxHealthMap = new Dictionary<Attackable, float>();
    private Dictionary<Attackable, float> currentHealthMap = new Dictionary<Attackable, float>();

    private static HealthManager instance = null;

    public static HealthManager GetInstance()
    {
        if (instance == null)
        {
            instance = new HealthManager();
        }
        return instance;
    }

    public void AddEntity(Attackable entity, float maxHealth)
    {
        maxHealthMap.Add(entity, maxHealth);
        currentHealthMap.Add(entity, maxHealth);
    }

    public void DamageEntity(Attackable entity, float damage)
    {
        currentHealthMap[entity] -= damage;

        HealthBar healthBar = entity.healthBar.GetComponent<HealthBar>();
        healthBar.UpdateHealth(currentHealthMap[entity] / maxHealthMap[entity]);

        if (currentHealthMap[entity] <= 0)
        {
            entity.Die();
        }
    }
    
}
