﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SpawnPositions
{
    private static Vector3 spawnPosition1 = new Vector3(-53, 0 - 13);
    private static Vector3 spawnPosition2 = new Vector3(-13, 0, -20);
    private static Vector3 spawnPosition3 = new Vector3(-34, 0, 0);
    private static Vector3 spawnPosition4 = new Vector3(4, 0, 21);
    private static Vector3 spawnPosition5 = new Vector3(48, 0, -16);
    private static Vector3 spawnPosition6 = new Vector3(68, 0, -22);
    private static Vector3 spawnPosition7 = new Vector3(91, 0, 0);
    private static Vector3 spawnPosition8 = new Vector3(26, 0, -21);
    private static Vector3 bossPosition = new Vector3(91, 0, -21);

    public static readonly List<Vector3> spawnPositionList = new List<Vector3>(){
                        spawnPosition1,
                        spawnPosition2,
                        spawnPosition3,
                        spawnPosition4,
                        spawnPosition5,
                        spawnPosition6,
                        spawnPosition7,
                        spawnPosition8
                    };

    public static readonly List<Vector3> bossPositionList = new List<Vector3>(){
                        bossPosition
                    };
}