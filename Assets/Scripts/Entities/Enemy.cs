using System.Collections;
using UnityEngine;

public class Enemy : Attackable
{
    [SerializeField]
    private float aggroRange;
    [SerializeField]
    private float speed;
    [SerializeField]
    private float range;
    [SerializeField]
    private float attackDamage;
    [SerializeField]
    private float attackCooldown;
    [SerializeField]
    private float maxHealth;
    [SerializeField]
    private string enemyName;
    [SerializeField]
    private float experience;

    private GameObject player;
    private ExperienceManager experienceManager;
    private Rigidbody rigidBody;
    private bool onCooldown = false;
    private HealthManager healthManager;
    private ItemManager itemManager;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        player = GameObject.Find("Player");
        healthManager = HealthManager.GetInstance();
        healthManager.AddEntity(this, maxHealth);
        itemManager = ItemManager.GetInstance();
        experienceManager = GameObject.Find("ExperienceBar").GetComponent<ExperienceManager>();
    }

    void FixedUpdate()
    {
        float distance = Vector3.Distance(player.transform.position, transform.position);

        if (distance < aggroRange)
        {

            if (distance <= range)
            {
                AttackPlayer();
            }
            else
            {
                Vector3 direction = (player.transform.position - transform.position).normalized;
                Move(direction);
            }
        }
    }

    private void AttackPlayer()
    {
        if (!onCooldown)
        {
            onCooldown = true;
            healthManager.DamageEntity(player.GetComponent<Player>(), attackDamage);
            StartCoroutine(AttackCooldown());
        }
    }

    IEnumerator AttackCooldown()
    {
        yield return new WaitForSeconds(attackCooldown);
        onCooldown = false;
    }

    private void Move(Vector3 direction)
    {
        Vector3 movement = direction * speed * Time.deltaTime;
        rigidBody.MovePosition(rigidBody.position + movement);

        float angle = Vector3.Angle(direction, transform.forward);
        if (angle > 10f)
        {
            transform.LookAt(new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z));
        }
    }

    public override void Die()
    {
        itemManager.Drop(enemyName, transform.position.x, transform.position.z);
        experienceManager.Add(experience);
        Destroy(gameObject);
    }
}
