﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Attackable : MonoBehaviour
{
    public GameObject healthBar;
    public abstract void Die();
}
