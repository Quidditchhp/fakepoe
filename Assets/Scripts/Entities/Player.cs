﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : Attackable
{
    private const float minRotationAngle = 10f;

    private Rigidbody rigidBody;
    private Class playerClass;

    private float maxHealth = 100;
    private float speed = 10f;
    private float sidewaysSpeed = 7f;
    private bool uiLock = false;

    public bool animationLocked = false;
    public GameObject inventory;

    private void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        HealthManager.GetInstance().AddEntity(this, maxHealth);

        playerClass = new Warrior();
    }

    private void FixedUpdate()
    {
        if (!animationLocked)
        {
            MovePlayer();
            Vector3 mousePosition = GetMousePosition();
            if (Input.GetKey(KeyCode.Mouse0))
            {
                ProcessClick(mousePosition);
            }
            if (Input.GetKey(KeyCode.Q))
            {
                playerClass.UseSkill(KeyCode.Q, this, mousePosition);
            }
            else if (Input.GetKey(KeyCode.E))
            {

            }
            else if (Input.GetKey(KeyCode.R))
            {

            }
            else if (Input.GetKey(KeyCode.F))
            {

            }
            else if (Input.GetKey(KeyCode.I) && !uiLock)
            {
                uiLock = true;
                if (inventory.activeInHierarchy)
                {
                    inventory.SetActive(false);
                }
                else
                {
                    inventory.SetActive(true);
                }

                StartCoroutine(UITimer());
            }
        }
    }

    private void ProcessClick(Vector3 mousePosition)
    {
        GameObject clickedObject = GetClickedObject();
        if (!clickedObject.CompareTag("Player"))
        {
            float distance = Vector3.Distance(mousePosition, transform.position);

            if (clickedObject.CompareTag("Item") && distance <= 2)
            {
                PickUpItem(clickedObject);
            }
        }
    }

    private void MovePlayer()
    { 
        Vector3 movementFoward = transform.forward * Input.GetAxis("Vertical") * speed * Time.deltaTime;
        Vector3 movementSideways = transform.right * Input.GetAxis("Horizontal") * sidewaysSpeed * Time.deltaTime;
        rigidBody.MovePosition(rigidBody.position + (movementFoward + movementSideways));
    }

    private RaycastHit GetClick()
    {
        var ray = UnityEngine.Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        Physics.Raycast(ray, out hit);
        return hit;
    }

    private Vector3 GetMousePosition()
    {
        RaycastHit hit = GetClick();
        return new Vector3(hit.point.x, transform.position.y, hit.point.z);
    }

    private GameObject GetClickedObject()
    {
        RaycastHit hit = GetClick();
        return hit.transform.gameObject;
    }

    private void PickUpItem(GameObject item)
    {
        inventory.GetComponent<InventoryManager>().PickUpItem(item);
    }

    public override void Die()
    {
        gameObject.SetActive(false);
    }

    IEnumerator UITimer()
    {
        yield return new WaitForSeconds(0.25f);
        uiLock = false;
    }
}