﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject guardPrefab;
    public GameObject captainPrefab;
    public GameObject bossPrefab;

    private const float spawnRange = 4;
    private const int packSize = 5; //size of a pack of enemies

    // Start is called before the first frame update
    private void Start()
    {
        SpawnEnemies();
    }
    private void SpawnEnemies()
    {
        foreach (Vector3 position in SpawnPositions.spawnPositionList)
        {
            SpawnEnemyWave(position);
        }

        foreach (Vector3 position in SpawnPositions.bossPositionList)
        {
            SpawnBoss(position);
        }
    }

    private Vector3 GenerateSpawnPosition(Vector3 center, float y)
    {
        float spawnX = Random.Range(-spawnRange, spawnRange);
        float spawnZ = Random.Range(-spawnRange, spawnRange);

        return new Vector3(center.x + spawnX, y, center.z + spawnZ);
    }

    private void SpawnEnemyWave(Vector3 position)
    {
        for (int i = 0; i < packSize; i++)
        {
            //Spawns 1 captain. The rest are normal guards
            if (i == 0)
            {
                Instantiate(captainPrefab, GenerateSpawnPosition(position, captainPrefab.transform.position.y), captainPrefab.transform.rotation);
            }
            else
            {
                Instantiate(guardPrefab, GenerateSpawnPosition(position, guardPrefab.transform.position.y), guardPrefab.transform.rotation);
            }
        }
    }

    private void SpawnBoss(Vector3 position)
    {
        Instantiate(bossPrefab, new Vector3(position.x, bossPrefab.transform.position.y, position.z), bossPrefab.transform.rotation);
    }
}
