﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Class
{
    public string name;
    public Dictionary<KeyCode, Skill> keybinds;
    public List<Skill> skills;

    public Class(string name, List<Skill> skills)
    {
        this.name = name;
        this.skills = skills;
        keybinds = new Dictionary<KeyCode, Skill>();
        SetupSkills();
        SetupKeybinds();
    }

    public abstract void SetupSkills();
    public abstract void SetupKeybinds();
    public void UseSkill(KeyCode key, Player player, Vector3 mousePosition)
    {
        keybinds[key].Use(player, mousePosition);
    }
}
