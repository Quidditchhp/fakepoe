﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Warrior : Class
{
    public Warrior() : base("Warrior", new List<Skill>())
    {
    }

    public override void SetupKeybinds()
    {
        keybinds.Add(KeyCode.Q, skills.First(skill => skill.name == "Heavy Strike"));
    }

    public override void SetupSkills()
    {
        skills.Add(new HeavyStrike());
    }
}
