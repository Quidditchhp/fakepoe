﻿using UnityEngine;

public class Camera : MonoBehaviour
{
    public GameObject target;

    private float x = 0;
    private float y = 0;

    private float distance = 10;

    private void Start()
    {
        Vector3 angles = transform.eulerAngles;
        x = angles.y;
        y = angles.x;
    }
    private void Update()
    {
        distance = Mathf.Clamp(distance - Input.GetAxis("Mouse ScrollWheel") * 5, 3, 15);
    }

    private void FixedUpdate()
    {
        if (Input.GetKey("mouse 1"))
        {
            Cursor.visible = false;
            x += Input.GetAxis("Mouse X") * 20 * distance * 0.02f;
            y -= Input.GetAxis("Mouse Y") * 20 * distance * 0.02f;
        }
        else
        {
            Cursor.visible = true;
        }

        y = ClampAngle(y, -20, 80);

        var rotation = Quaternion.Euler(y, x, 0);

        RaycastHit hit = new RaycastHit();
        Vector3 upperBound = new Vector3(target.transform.position.x, target.transform.position.y + 2, target.transform.position.z);
        if (Physics.Linecast(upperBound, transform.position, out hit))
        {
            if (!hit.transform.gameObject.CompareTag("Enemy"))
            {
                distance -= (hit.distance - 5);
            }
        }

        var position = rotation * new Vector3(0, 0, -distance) + target.transform.position;

        target.transform.rotation = Quaternion.Euler(0, x, 0);
        transform.rotation = rotation;
        transform.position = position;
        
    }
    private float ClampAngle(float angle, float min, float max)
    {
        if (angle < -360)
            angle += 360;
        if (angle > 360)
            angle -= 360;
        return Mathf.Clamp(angle, min, max);
    }
}
